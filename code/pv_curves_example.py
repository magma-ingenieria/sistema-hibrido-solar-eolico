import matplotlib.pyplot as plt
from Minotauro import PhotovoltaicPanel as PV


def run():
    panel = PV()
    Vi, Iv, Pot = panel.panel_curves()

    fig, (ax1, ax2) = plt.subplots(2, 1)
    ax1.plot(Vi, Iv)
    ax1.set_ylabel('Current (A)')

    ax2.plot(Vi, Pot)
    ax2.set_xlabel('Voltage (V)')
    ax2.set_ylabel('Power (KW)')

    plt.show()


if __name__ == '__main__':
    run()
