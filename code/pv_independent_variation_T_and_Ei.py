import matplotlib.pyplot as plt
from Minotauro import PhotovoltaicPanel as PV


def run():
    panel_irradiation = PV()
    panel_temperature = PV()

    irradiation = []
    power_irradiation = []

    temperature = []
    power_temperature = []

    for x in range(0, 5000, 10):
        panel_irradiation.Ei = x
        irradiation.append(panel_irradiation.Ei)

        panel_irradiation.Vi += 0.01
        panel_irradiation.model_init()
        power_irradiation.append(panel_irradiation.model_current())

    for x in range(181):
        panel_temperature.T = x
        temperature.append(panel_temperature.T)

        panel_temperature.Vi += 0.01
        panel_temperature.model_init()
        power_temperature.append(panel_temperature.model_power())

    fig, (ax1, ax2) = plt.subplots(2, 1)
    ax1.plot(irradiation, power_irradiation)
    ax1.set_xlabel('Irradiation (KW/m^2)')
    ax1.set_ylabel('Power (KW)')

    ax2.plot(temperature, power_temperature)
    ax2.set_xlabel('Temperature (°C)')
    ax2.set_ylabel('Power (Kw)')

    plt.show()


if __name__ == '__main__':
    run()
