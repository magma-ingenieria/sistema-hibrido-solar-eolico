import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D

def run():
    df = pd.read_excel('../data_set/salamanca_modulo_pv.xlsx')

    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')

    x = df['radiacionSolarNormalizada'][:1000]
    y = df['temperaturaNormalizada'][:1000]
    z = df['potenciaPVNormalizada'][:1000]

    ax.set_xlabel("radiacionSolar")
    ax.set_ylabel("temperatura")
    ax.set_zlabel("potenciaPV")

    ax.scatter(x, y, z)

    plt.show()


if __name__ == '__main__':
    run()
