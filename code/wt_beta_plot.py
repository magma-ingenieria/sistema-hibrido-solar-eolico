import matplotlib.pyplot as plt
from Minotauro import WindTurbine as Turbine


def run():
    turbine = Turbine()
   
    list_lambda0 = []
    list_cp = []

    for turbine.beta in range(0, 25, 5):
        lambda0, cp = turbine.find_cp()
        turbine.lambda0 = 0.0
        list_lambda0.append(lambda0)
        list_cp.append(cp)

    plt.plot(list_lambda0[0], list_cp[0])
    plt.plot(list_lambda0[1], list_cp[1])
    plt.plot(list_lambda0[2], list_cp[2])
    plt.plot(list_lambda0[3], list_cp[3])
    plt.plot(list_lambda0[4], list_cp[4])
    plt.show()


if __name__ == '__main__':
    run()
