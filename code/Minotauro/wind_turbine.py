import numpy as np


class WindTurbine:
    def __init__(self, cp=0.1, rho=1.225, area=1.0751, velwind=0.0, c1=0.5176, c2=116, c3=0.4, c4=5, c5=21, c6=0.0068, lambda0=0.0, beta=0.0, lambdai=1.0):
        self.cp = cp
        self.rho = rho
        self.area = area
        self.velwind = velwind
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.c4 = c4
        self.c5 = c5
        self.c6 = c6
        self.lambda0 = lambda0
        self.beta = beta
        self.lambdai = lambdai

    def power(self):
        self.Pot = 0.5*self.cp*self.rho*self.area*np.power(self.velwind, 3)
        return self.Pot

    def lambdaiter(self):
        self.x1 = 1/(self.lambda0 + 0.08*self.beta)
        self.x2 = 0.035/(np.power(self.beta, 3) + 1)
        self.x3 = 1/(self.x1 - self.x2)
        return self.x3

    def power_coefficient(self):
        self.cp = self.c1*((self.c2/self.lambdai) - self.c3*self.beta -
                           self.c4)*(np.exp(-self.c5/self.lambdai)) + self.c6*self.lambda0
        return self.cp

    def find_cp(self, step=15):
        self.list_lambda0 = []
        self.list_cp = []

        for i in range(step):
            self.lambda0 += 1
            self.lambdai = self.lambdaiter()
            self.cp = self.power_coefficient()

            self.list_lambda0.append(self.lambda0)
            self.list_cp.append(self.cp)

        return self.list_lambda0, self.list_cp

    def power_curve(self, step=35, limit=25):
        self.list_Pot = []
        self.list_velwind = []

        for i in range(step):
            if self.velwind >= limit:
                self.velwind = limit

            self.list_Pot.append(self.power())
            self.list_velwind.append(i)

            self.velwind += 1

        self.list_Pot.append(0)
        self.list_velwind.append(step + 0.25)

        return self.list_velwind, self.list_Pot
