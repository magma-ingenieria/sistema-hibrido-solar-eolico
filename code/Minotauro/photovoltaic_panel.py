import numpy as np


class PhotovoltaicPanel:
    '''
    Ortiz model for photovoltaic panel.

    Ideal panel

    '''

    def __init__(self, Vi=0, s=1, p=1, Ei=1000, Ein=1000, Tn=25, b=0.0684, Isc=3.71, Voc=21.40, T=25, TCv=-0.1261, TCi=0.00418):
        self.Vi = Vi
        self.s = s
        self.p = p
        self.Ei = Ei
        self.Ein = Ein
        self.Tn = Tn
        self.b = b
        self.Isc = Isc
        self.Voc = Voc
        self.T = T
        self.TCv = TCv
        self.TCi = TCi
        self.Vmax = self.Voc*1.03
        self.Vmin = self.Voc*0.5

    def short_circuit_current(self):
        self.Ix = self.p*(self.Ei/self.Ein)*(self.Isc +
                                             (self.TCi*(self.T - self.Tn)))
        return self.Ix

    def open_circuit_voltage(self):
        self.Vx = self.s*(self.Ei/self.Ein)*self.TCv*(self.T-self.Tn) + (self.s*self.Vmax) - (self.s*(
            self.Vmax - self.Vmin))*np.exp((self.Ei/self.Ein)*np.log((self.Vmax - self.Voc)/(self.Vmax - self.Vmin)))
        return self.Vx

    def model_init(self):
        self.short_circuit_current()
        self.open_circuit_voltage()

    def model_current(self):
        self.Iv = (self.Ix/(1 - np.exp((-1/self.b)))) * \
            (1-(np.exp((self.Vi/(self.b*self.Vx)) - (1/self.b))))
        return self.Iv

    def model_power(self):
        self.Pot = self.model_current()*self.Vi
        return self.Pot

    def panel_curves(self, step=0.01):
        self.Vi = step
        self.finc = self.open_circuit_voltage()/step
        self.model_init()

        self.list_Vi = []
        self.list_Iv = []
        self.list_Pot = []

        for i in range(int(self.finc)):
            self.Iv = self.model_current()
            self.Pot = self.model_power()

            self.list_Vi.append(self.Vi)
            self.list_Iv.append(self.Iv)
            self.list_Pot.append(self.Pot)

            self.Vi += step

        return self.list_Vi, self.list_Iv, self.list_Pot
