import random
import matplotlib.pyplot as plt
from Minotauro import PhotovoltaicPanel as PV


def run():
    panel = PV()

    temperature = []
    irradiation = []
    power = []
    sample = []

    for i in range(100):
        sample.append(i)

        panel.T = random.randrange(20, 40)
        panel.Ei = random.randrange(4000, 6000)

        temperature.append(panel.T)
        irradiation.append(panel.Ei)

        panel.Vi = 1
        panel.model_init()
        power.append(panel.model_power())

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    ax1.plot(sample, power)
    ax1.set_ylabel('Power (KW)')

    ax2.plot(sample, irradiation)
    ax2.set_ylabel('Irradiation (KW/m^2)')

    ax3.plot(sample, temperature)
    ax3.set_ylabel('Temperature (°C)')
    ax3.set_xlabel('Sample')

    plt.show()


if __name__ == '__main__':
    run()
