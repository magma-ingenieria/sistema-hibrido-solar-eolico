import pandas as pd
import matplotlib.pyplot as plt
from Minotauro import PhotovoltaicPanel as PV


STEPS = 1000


def load_data():
    df = pd.read_excel(r'data_set/islasalamanca.xlsx')
    df.drop(['station', 'fecha', 'rh', 'dewPoint', 'presion', 'velocidadViento',
             'velocidadRafaga', 'direccionViento', 'lluvia', 'humedadHoja'], axis=1, inplace=True)

    return df


def run():
    panel = PV(Vi=12)

    power = []

    df = load_data()
    temperature = tuple(df['temperatura'][:STEPS])
    irradiation = tuple(df['radiacionSolar'][:STEPS])
    voltage = []
    current = []

    for t, ei in zip(temperature, irradiation):
        panel.T = t
        panel.Ei = ei

        panel.model_init()
        voltage.append(panel.open_circuit_voltage())
        current.append(panel.model_current())
        power.append(panel.open_circuit_voltage()*panel.model_current())

    sample = list(range(STEPS))

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    ax1.plot(sample, power)
    ax1.set_ylabel('Power (KW)')

    ax2.plot(sample, irradiation)
    ax2.set_ylabel('Irradiation (KW/m^2)')

    ax3.plot(sample, temperature)
    ax3.set_ylabel('Temperature (°C)')
    ax3.set_xlabel('Sample')

    plt.show()


if __name__ == '__main__':
    run()
