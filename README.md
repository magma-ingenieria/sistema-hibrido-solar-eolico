# Minotauro Project

Modeling of all the elements that make up a hybrid solar-wind system, integration of its components and 
some control algorithms.

## Install packages:
`python -m pip install -r requirements.txt`
> **Note:** use `python3` in linux.